# Challenge IA - Projet Santé

## Equipe
- Romé Levenq
- Pierre-Louis Martin-Chivico
- Rayan Smaoui
- Martin Vey
- Maxime Zennaro

## Table of contents
1. [Description du Projet](#description-du-projet)
2. [Installation](#installation)
3. [Structure du Projet](#struct-projet)
4. [Downloads for concept prediction](#concept-pred)
5. [Support](#support)


## Description du Projet
Dans le cadre d’essais cliniques, il est nécessaire de constituer des groupes de patients éligibles pour y participer. Ces groupes sont appelés des cohortes de patients. 
Le projet consiste à développer un outil intelligent afin d'automatiser l'affectation d'un patient à un groupe selon les informations contenus dans les rapport médicaux. Une cohorte est associée à un type de pathologie.
Pour ce faire, des technologies de Natural Language Processing seront utilisés. 


## Installation

- pip install pandas
- pip install numpy
- pip install gensim
- pip install matplotlib
- pip install nltk
- pip install wordcloud
- pip install torch
- pip install transformers


## Structure du Projet

Notre répertoire projet comporte :
- un fichier README
- un jupyter notebook intitulé "code_concepts" relatif à l'extraction des concepts médicaux à partir de rapport médicaux de patients
- un jupyter notebook intitulé "assertions" relatif à l'élaboration des assertions i.e. identifier l'état d'une pathologie ou d'un traitement pour un patient
- un jupyter notebook intitulé "clustering" sur le clustering afin de mettre en place des cohortes de patients partageant les mêmes afflictions. 
- un dossier principal "Health Data Challenge" contenant les différents sous-dossiers suivants :
    - les dossiers d'entrainements, de validation et de tests (avec les 4 types de fichiers que sont les fichiers .txt, .con, .rel et les .ast)


## Download models 
- Please download fine-tuned models [here](https://drive.google.com/file/d/1PPXos4D-ZXYbBzpAqyNOP9qWFIhV1KmM/view?usp=sharing). Put the .pt files in the same folder as code_concepts.ipynb
- Please download the model for assertion prediction [here](https://drive.google.com/file/d/1_YfYXm5TlBkSUEczGnlSGn1oBtkZf93Q/view?usp=sharing). Put the model in the same folder as Assert_val.ipynb.  
- This instruction is not necessary beacause you can directly load the model in clustering.ipynb. But if you want to retrain the model, please download the trained W2V model [model_W2V](https://drive.google.com/file/d/1ur8gw_4YXiSsEOHsGhEP6us2fYSqBUz4/view?usp=sharing). Create a folder called "w2v_model" in Health Data Challenge and put the downloaded files in it.

## Support
- Jupyter Notebook
- IDE : VScode and python 3.7+ base conda
- Environnement : GitLab CS
